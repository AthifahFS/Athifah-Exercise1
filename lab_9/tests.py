from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, set_data_for_session, profile, response as response_index, my_cookie_auth
from .api_enterkomputer import  get_drones
from .csui_helper import get_access_token, get_client_id, verify_user, get_data_user
from .custom_auth import auth_login, auth_logout

import environ
import requests

root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env('.env')

# Create your tests here.
class Lab9UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_lab_9_url_is_exist(self):
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)

        # logged in, redirect to profile page
        self.assertNotIn("user_login", self.client.session)
        html_content = response.content.decode('utf-8')
        self.assertIn("Halaman Login", html_content)

        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        login = self.client.get('/lab-9/')
        self.assertEqual(login.status_code, 302)
    
    def test_profile(self):
        # Not logged in
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

        # Logged in
        self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

    def test_set_data(self):  # baru kondisi else
        self.client.session['drones'] = response_index['drones']
       # response = self.client.post('/lab-9/custom_auth/login/')
        self.assertEqual(response_index['fav_drones'], [])

    def test_lab9_using_index_func(self):   
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_drones(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        response = self.client.post('/lab-9/add_session_drones/' + get_drones().json()[0].get("id") + '/')
        response = self.client.post('/lab-9/add_session_drones/' + get_drones().json()[1].get("id") + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah drone favorite", html_response)

        response = self.client.post('/lab-9/del_session_drones/' + get_drones().json()[0].get("id") + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus dari favorite", html_response)

        response = self.client.post('/lab-9/clear_session_drones/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil reset favorite drones", html_response)

    
    def test_cookie_login(self):
        cookie_not_login = self.client.post('/lab-9/cookie/login/')
        self.assertEqual(cookie_not_login.status_code, 200)
        self.assertIn('Login menggunakan COOKIES', cookie_not_login.content.decode('utf8'))

        cookie_login = self.client.post('/lab-9/cookie/auth_login/', {'username': 'athifah', 'password': 'hehehe'})
        cookie_is_login = self.client.post('/lab-9/cookie/login/')
        self.assertEqual(cookie_is_login.status_code, 200)

    def test_cookie_auth_login(self):
        method_get = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(method_get.status_code, 302)

        wrong_input = self.client.post('/lab-9/cookie/auth_login/', {'username': 'abc', 'password': 'def'})
        self.assertEqual(wrong_input.status_code, 302)
        self.assertRaisesMessage("Username atau Password Salah", wrong_input)

        correct_input = self.client.post('/lab-9/cookie/auth_login/', {'username': 'athifah', 'password': 'hehehe'})
        self.assertEqual(correct_input.status_code, 302)

    def test_cookie_profile(self):
        not_logged_in = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(not_logged_in.status_code, 302)

        self.client.cookies.load({'user_login': 'u', 'user_password': 'p'})
        logged_in_wrong_input = self.client.get('/lab-9/cookie/profile/')
        self.assertRaisesMessage(logged_in_wrong_input, "Kamu tidak punya akses :P ")
        self.assertEqual(logged_in_wrong_input.status_code, 200)
        self.client.post('/lab-9/cookie/auth_login/',
                         {'username': 'athifah', 'password': 'hehehe'})

    #    correct_input = self.client.get('/lab-9/cookie/profile/')
    #    self.assertEqual(correct_input.status_code, 200)
    #    self.assertIn("[Cookie] Profile", correct_input.content.decode('utf8'))

    def test_cookie_clear(self):
        cookie_clear = self.client.get('/lab-9/cookie/clear/')
        self.assertEqual(cookie_clear.status_code, 302)
        self.assertRaisesMessage(cookie_clear, "Anda berhasil logout. Cookies direset")

    #def test_my_cookie_auth(self):
    #    result_true = my_cookie_auth('athifah', 'hehehe')
    #    self.assertTrue(result_true)
    #    result_false = my_cookie_auth('athifah', 'abcd')
    #    self.assertFalse(result_false)

    
    #test drones
    def test_get_drones(self):
        response = get_drones()
        self.assertEqual('<Response [200]>', str(response))
       # self.assertEqual(type(response), requests.Response)

    #test csui_helper
    def test_get_access_token(self):
        username = "abc"
        password = "efg"

        test_true= get_access_token(username,password)
        self.assertTrue(type(test_true),str)
        self.assertIsNone(test_true)

        test_false = get_access_token(self.username, self.password)
        self.assertIsNotNone(test_false)
    
    def test_get_client_id(self):
        client_id = get_client_id()
        self.assertTrue(type(client_id), str)
        self.assertEqual(client_id, "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")

    def test_verify_user(self):
        result = verify_user('1234567')
        self.assertIn("error_description",result)

    def test_get_data_user(self):
        res = get_data_user('1234567','234')
        self.assertIn("detail",res)

    #test custom_auth.py
    def test_auth_login(self):
        login = self.client.post('/lab-9/custom_auth/login/', {"username": self.username, "password": self.password})
        login2 = self.client.post('/lab-9/custom_auth/login/', {"username": "abcd", "password": "efgd"})
        access_token = get_access_token(self.username, self.password)
       # self.assertIsNone(access_token)
        
        self.assertIsNotNone(access_token)
        self.assertRaisesMessage("username atau password salah",login)
        self.assertEqual(login.status_code, 302)


    def test_auth_logout(self):
        login = self.client.post('/lab-9/custom_auth/login/', {"username": self.username, "password": self.password})
        logout = self.client.get('/lab-9/custom_auth/logout/')
        self.assertEqual(logout.status_code, 302)
        self.assertRaisesMessage("Anda berhasil logout. Semua session Anda sudah dihapus", logout)

