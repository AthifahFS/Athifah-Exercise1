// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '878228202344603',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render dibawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka, dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Rubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      $('.mainContainer').hide();
      // Jika yang akan dirender adalah tampilan sudah login

      // Panggil Method getUserData yang anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#login').hide();
        $('#lab8').html(
          '<div class="profile" style="background-image:url('+user.cover.source+')">' +
            '<div class="keluar"><img class="logoutImage" src='+pic1+' onclick="facebookLogout()"></div>' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<hr style="width:50%;" />' +
              '<h1>' + user.name + '</h1>' +
              '<h2> "' + user.about + '" </h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<div class="wraper1"><div class="wraper2">'+
          '<div class="wraper3"><input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" /></div>' +
          '<button class="postStatus" onclick="postStatus()">Post</button></div></div>' +
          '<div class="wraper4"></div>'
        );

        // Setelah merender tampilan diatas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            var waktu = date(value)
            var pk = getId(value.id)
            if (value.message && value.story) {
              $('.wraper4').append(
                '<div class="feed" id='+pk[1]+'>' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                  '<h5>' + waktu + '</h5>' +
                  '<button class="delPost" onclick=deleteFeed('+pk[0]+','+pk[1]+')>Delete</button>' +
                '</div>'
              );
            } else if (value.message) {
              $('.wraper4').append(
                '<div class="feed" id='+pk[1]+'>' +
                  '<h1>' + value.message + '</h1>' +
                  '<h5>' + waktu + '</h5>' +
                  '<button class="delPost" onclick=deleteFeed('+pk[0]+','+pk[1]+')>Delete</button>' +
                '</div>'
              );
            } else if (value.story) {
              $('.wraper4').append(
                '<div class="feed" id='+pk[1]+'>' +
                  '<h2>' + value.story + '</h2>' +
                  '<h5>' + waktu + '</h5>' +
                  '<button class="delPost" onclick=deleteFeed('+pk[0]+','+pk[1]+')>Delete</button>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada.
  FB.login(function(response){
    console.log(response);
    if(response.status === 'connected'){
      render(response);
    }
  }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})

  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses.
    FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
            FB.logout();
            window.location = redirect;
          }
      });
  };

  const getUserData = (user) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data User dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
     FB.getLoginStatus(function(response) {
       var token = response.authResponse.accessToken;
       var id = response.authResponse.userID
       if (response.status === 'connected') {
         FB.api('/'+id, 'GET', { access_token: token, fields: 'id,name,gender,cover,email,about,picture.width(164).height(164)' }, function(response){
           console.log(response);
           console.log(id);
           user(response);
         });
       }
     });
  };

  const getUserFeed = (feed) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
  FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.api('/me/feed', 'GET', function(response){
              console.log(response);
              feed(response);
            });
        }
    });
  };

  const postFeed = (message) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            FB.api(
                "/me/feed",
                "POST",
                {
                    message: message
                },
                function (response) {
                  if (response && !response.error) {
                        FB.api('/me?fields=feed', 'GET', function(responses){
                          var newFeed = responses.feed.data[0];
                          var waktu = date(newFeed)
                          var pk = getId(responses.feed.data[0].id)
                            $('.wraper4').prepend(
                              '<div class="feed" id='+pk[1]+'>' +
                                '<h1>' + newFeed.message + '</h1>' +
                                '<h5>' + waktu + '</h5>' +
                                '<button class="delPost" onclick=deleteFeed('+pk[0]+','+pk[1]+')>Delete</button>' +
                              '</div>'
                            );
                        });
                  }
                  else {
                    console.log(response);
                  }
                }
            );
        }
    });
  };

  const deleteFeed = (key, id) => {
    FB.getLoginStatus(function(response){
      if (response.status === 'connected') {
        var url = "/" + key + "_" + id;
        var token = response.authResponse.accessToken;
        FB.api(
            url,
            "DELETE",
            { access_token: token },
            function (response) {
              if (response && !response.error) {
                $('#'+id).fadeOut("fast");
              } else {
                alert("Gagal, bukan post dari aplikasi!!!")
              }
            }
        );
      }
    });
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
    event.preventDefault();
    $('#postInput').val("");
  };

  const date = (value) => {
    var monthNames = [
      "January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    var date = new Date((value.created_time).split('+')[0]+'Z')
    var waktu = date.getDate()+" "+monthNames[date.getMonth()]+" "+date.getFullYear()
    return waktu;
  }

  const getId = (value) => {
    return value.split('_')
  }
//
// Dengan menambahkan permission user_posts dan publish_actions, apa saja yang dapat dilakukan oleh aplikasi kita menggunakan Graph API?
// Apakah yang dimaksud dengan fungsi callback?
